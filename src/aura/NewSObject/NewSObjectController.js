({
    // this method is called when component initializes
    onInit: function( component, event, helper ) {
        let pageRef = component.get( 'v.pageReference' );
        console.log('pageRef: ' + JSON.stringify(pageRef));
        let eventParamMap = {
            //'defaultFieldValues' : {}
        };
		var defaultFieldValues = component.get('v.defaultFieldValues');
        if (pageRef != null && pageRef != undefined) {
            console.log('detect attributes');
            let attributes = pageRef.hasOwnProperty('attributes') ? pageRef.attributes : null;
            console.log('page attributes: ' + JSON.stringify(attributes));
            let state = pageRef.hasOwnProperty('state') ? pageRef.state : null;
            console.log('page state: ' + JSON.stringify(state));
            let recordTypeId = null;
            if (state != null && state != undefined) {
                recordTypeId = state.hasOwnProperty('recordTypeId') ? state.recordTypeId : null;
            	console.log('recordTypeId: ' + JSON.stringify(recordTypeId));
                if (state.hasOwnProperty('c__objectApiName')) {
                    for (let key in state) {
                        console.log('key = ' + key);
                        if (key == 'c__objectApiName' || key =='uid') {
                            continue;
                        }
                        else if (key == 'c__recordTypeId') {
                            recordTypeId = state['c__recordTypeId'];
                        } else {
                            var newKey = key;
                            if (key != null && key != undefined && 
                                key.length > 3 && key.substring(0,3) == 'c__') {
                                newKey = key.substring(3, key.length);
                            }
                            defaultFieldValues[newKey] = state[key];
                            console.log('key for default: ' + key);
                        }
                    }
                }
            }
            eventParamMap['entityApiName'] = attributes.hasOwnProperty('objectApiName') ?attributes.objectApiName
            : (state.hasOwnProperty('c__objectApiName') ? state['c__objectApiName']: null);
            
            eventParamMap['recordTypeId'] = recordTypeId != null ? recordTypeId :
                (state.hasOwnProperty('c__recordTypeId') ? state['c__recordTypeId']: null);
            eventParamMap.defaultFieldValues = defaultFieldValues;
            console.log('eventParaMap: ' + JSON.stringify(eventParamMap));
            setTimeout(function(){ $A.get( 'e.force:createRecord' ).setParams( eventParamMap ).fire(); }, 1000);
        }
        
    }
})